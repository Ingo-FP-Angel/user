# How to run

```shell
$ git clone
$ cd wiki
$ sudo docker run --rm --volume="$PWD:/srv/jekyll" -p 4000:4000  -it jekyll/jekyll:4.2.0 jekyll serve
```

default template (if needed to override): https://github.com/jekyll/minima
documentation: https://jekyllrb.com/docs

Generate for a device

```shell
$ git clone
$ cd wiki
$ ./scripts/generate_device.sh starlte "Galaxy S9" Samsung oreo "dev, stable"
```

pay attention to file user, a `chmod -R 1000:1000 wiki` could help 

To build user this command as an example:

manoj@RIANControl:~/user$ docker run -it --rm --volume="/home/manoj/user:/srv/jekyll" -p 4000:4000  -it jekyll/jekyll:4.2.0 jekyll serve

Replace here /home/yourhomefoldername/