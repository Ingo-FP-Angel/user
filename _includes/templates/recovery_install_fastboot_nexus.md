## Unlocking the bootloader

{% include alerts/warning_unlocking_bootloader.md %}

{% unless device.no_oem_unlock_switch %}
1.&nbsp;Enable OEM unlock in the Developer options under device Settings, if present.
{% endunless %}
2.&nbsp;Connect the device to your PC via USB.

3.&nbsp;On the computer, open a command prompt (on Windows) or terminal (on Linux or macOS) window, and type:
```
adb reboot bootloader
```
{% if device.download_boot %}
You can also boot into fastboot mode via a key combination:

* {{ device.download_boot }}
{% endif %}


4.&nbsp;Once the device is in fastboot mode, verify your PC finds it by typing:
```
fastboot devices
```
{% include alerts/tip.html content="If you see `no permissions fastboot` while on Linux or macOS, try running `fastboot` as root." %}

5.&nbsp;Now type the following command to unlock the bootloader:

{% if device.custom_unlock_cmd %}
```
{{ device.custom_unlock_cmd }}
```
{% else %}
```
fastboot oem unlock
```
{% endif %}

6.&nbsp;If the device doesn't automatically reboot, reboot it. It should now be unlocked.

7.&nbsp;Since the device resets completely, you will need to **re-enable USB debugging** to continue.

{% if device.is_ab_device and device.has_recovery_partition != true %}
{% include templates/recovery_install_fastboot_ab.md %}
{% else %}
{% include templates/recovery_install_fastboot_generic.md %}
{% endif %}