## Downloads for the {{ device.codename }}

{% if device.uses_twrp %}
{% include alerts/tip.html content="Download of TWRP is not required if you already have it running on your phone " %}   
- [TWRP](https://dl.twrp.me/{{ device.codename }})
{% endif %}


{% if device.twrp_version %}
> The version we used for our testing was {{ device.twrp_version }}
{% endif %}
{% if device.e_recovery %}
Or try the /e/ Recovery (**limited functionality**)

- [/e/ Recovery](https://images.ecloud.global/{{device.status}}/{{device.codename}})
{% endif %}
{% if device.alternate_twrp %}
> In case of issues with TWRP:
- Alternate TWRP : {{ device.alternate_twrp }}
{% endif %}
{% if device.custom_recovery_link %}
In case the TWRP for you device does not exist or is not working as expected try this custom recovery

- [Custom recovery]({{ device.custom_recovery_link }})
{% endif %}
{% if device.uses_lineage_recovery %}
- [Lineage Recovery ](https://download.lineageos.org/{{ custom_recovery_codename }})
{% endif%}
{% if device.additional_patch %}

Additional Patches :
Flash this [patch]({{ device.additional_patch }}) on your {{ device.codename }} before flashing /e/OS

{% endif %}
{% if device.codename == "cheeseburger" %}

Latest [firmware zip file](https://www.androidfilehost.com/?fid=17248734326145729701)
{% endif %}
{% if device.codename == "dumpling" %}
- Latest [firmware zip file](https://www.androidfilehost.com/?fid=17248734326145729708)
{% endif %}
{%- for status in device.status %}
- /e/OS build : [{{status}}](https://images.ecloud.global/{{status}}/{{device.codename}})
{%- endfor %}

{%- if device.before_e_install %}
<br>
- Check the **Pre-Install Instructions** section below for some additional downloads for your device.
{% endif  %}

{% if device.codename == "cheeseburger" %}
- Download Firmware : [link](https://forum.xda-developers.com/t/index-oneplus-5-cheeseburger.3810283/)    
{% endif %}

{% if device.codename == "GS290" %}
- Additional images:
- [recovery.img](https://images.ecloud.global/dev/GS290/recovery-e-0.14-q-2021012698290-dev-GS290.img) ([sha256sum](https://images.ecloud.global/dev/GS290/recovery-e-0.14-q-2021012698290-dev-GS290.img.sha256sum))

- [boot.img](https://images.ecloud.global/dev/GS290/boot.img) ([sha256sum](https://images.ecloud.global/dev/GS290/boot.img.sha256sum))

- Download [Android 10 firmware](https://www.androidfilehost.com/?fid=17248734326145687642)
> only if reverting to Stock ROM
{% endif %}

{% if device.vendor == 'Xiaomi' %}
- MiUI Unlock tool [link](https://en.miui.com/unlock/download_en.html)

- [MiUI Flash Tool](https://xiaomiflashtool.com/)
{% if device.stockupgrade_toolver %}
    The version we tested was {{ device.stockupgrade_toolver }}
{% endif %}
{% endif %}

{% if device.vendor == 'Xiaomi' %}
{%  include alerts/tip.html content="Xiaomi users when reverting to the stock MiUi ROM use the Mi Flash Tool. This requires a windows PC. Linux users can try an alternate method to flash the stock ROM. They can unzip the ROM file and run the flash-all.sh file from a console screen. " %}
{% endif %}

{% if device.stockupgrade %}
- Stock ROM : [here]({{ device.stockupgrade }})
{% if device.stockupgrade_tool %}
- Stock flashing tool : [here]({{ device.stockupgrade_tool }})
{% if device.stockupgrade_toolver %}
- Tool version tested {{ device.stockupgrade_toolver }}
{% endif %}
{% endif %}
- Follow the steps to install the stock ROM as given on the vendor site
- Once the Stock ROM is flashed check if it is working for e.g receiving and making calls, audio, FMRadio ..
- Next enable developer options on the phone
- Ensure the device bootloader is unlocked. Flashing stock ROM can lock the bootloader on some devices.
- Follow the device unlock guidelines as prescibed by your vendor. For e.g. on Xiaomi devices it would require running the Mi Unlock tool. Since the device was already unlocked the process should take a couple of minutes only.
{% endif %}


{% if device.codename == 'wt88047' %}
- Download the relevant firmware zip file from [here](https://www.androidfilehost.com/?w=files&flid=303434).
> To get the details of the relevant firmware read the next section **Updating firmware**
{% endif %}

<p class="text-danger">Alert: Please note some of the above links can lead to external sites</p>
