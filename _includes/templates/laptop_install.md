
<div class="wrapper" markdown="1">

{% assign device = site.data.laptops[page.device] %}

## Install /e/OS on your {{ device.codename }} {{ device.type }}

### /e/OS code sources for this laptop :

{% if device.gitlocation %}

You can download the /e/OS for PineBook [here]( {{ device.gitlocation }} )

{% endif %}

### Download the Build

Get the /e/ OS build to flash on your {{ device.type }} [here]({{ device.stockupgrade }})

### Installation guide

{% if device.codename == 'pinebook' %}
  {% include templates/laptop_install_pinebook.md %}
{% elsif device.codename == 'olimex' %}
  {% include templates/laptop_install_olimex.md %}
{% else %}
  Laptop not supported :(
{% endif %}


Please note: In case you run  into issue with the installation steps or find any step missing please let us know by sending a mail to <support@e.email> and we will have the details added.
