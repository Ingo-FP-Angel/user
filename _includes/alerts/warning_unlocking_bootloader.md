<div class="alert alert-info">
    <div class="d-inline ml-1" markdown="span"><b class="mr-3">Tip:</b>The steps given below only need to be run once per device.</div>
</div>

<div class="alert alert-warning">
<div class="d-inline ml-1" markdown="span"><b class="mr-3">Warning:</b>
Unlocking the bootloader will erase all data on your device! Before proceeding, ensure the data you would like to retain is backed up to your PC and/or an online drive
</div>
</div>

