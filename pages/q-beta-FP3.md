---
layout: page
title: Android Q beta for FP3 and FP3+
permalink: q-beta-fp3
---

/e/ now supports officially FP3/FP3+ with Android Q! Kindly refer to the [official documentation](devices/FP3/install) to install it.
