---
layout: page
title: Report an issue
permalink: how-tos/report-an-issue
search: exclude
---

What to do when you encounter an issue?

### 1. Connect to our GitLab server

* If you have an account, [login](https://gitlab.e.foundation/users/sign_in)
* If not, [create one](https://gitlab.e.foundation/e)

### 2. Go to our issue dashboard

- /e/ issues are listed and created [here](https://gitlab.e.foundation/e/backlog/-/issues)

- Before creating a new issue, please search if it has previously been reported. To check for this

- Type the bug in the search text box on the top of the screen as shown below

![New Search](../../../images/new_issue_search.png)

- The screen will also display some other similar worded issues

- If this search does not return any results create a new issue

### 3. Create a new issue

- Select a template for the issue from the drop down marked 'Choose a template'
- 'Apply template'
- Add a Title
- Fill in all the details as requested in the template
- Always [add logs](https://community.e.foundation/t/howto-get-a-log-from-your-phone/2366) where possible. This helps the developers debug the issue better.
- Once you have finished, click **Submit issue**.


### 4. Syntax and Source

* [Source](https://docs.gitlab.com/ee/user/project/description_templates.html#setting-a-default-template-for-issues-and-merge-requests)
* [Markdown (GitLab)](https://docs.gitlab.com/ee/user/markdown.html)
* [Markdown Guide  (GitLab)](https://about.gitlab.com/handbook/product/technical-writing/markdown-guide/)
