---
layout: page
title: Enable USB Debugging
permalink: /pages/enable-usb-debugging
search: exclude
toc: true
---

## To enable USB Debugging

- You will need to become a `Developer` first on your device !!

## To become a developer
- Unlock your device and go to 

`Settings` > `About Phone` > `Build Number or Version` > `Tap seven times`


You will get a message that you are a Developer.

## To Allow USB Debugging
After completing the above steps and becoming a developer on your device go to 


`Settings` > `Additional Settings` > `Developer Options` > `Toggle Developer Options to enable` > `Toggle USB Debugging to enable`

On some devices the path may be as under 

`Settings` >> `System` >> `Developer options` >> `USB Debugging ( enable it here)`


Return to main screen