---
layout: page
title: Testing @/e/
permalink: testers
search: exclude
toc: true
---
### Types of Testing
At /e/ we have three types of testing
 - Sprint testing
 - Upgrade testing
 - Porting or Applications testing

**Sprint Testing** This is the regular testing for our builds. This testing is done for all devices in our [supported list](https://doc.e.foundation/devices/#smartphones-list)
We follow the agile model of development where we have regular sprints. 2 weeks of development and 1 week of testing. [Read more](https://community.e.foundation/t/e-os-and-the-art-of-remote-project-management/11636)


**Upgrade Testing** When the OS on a device is to be upgraded - from nougat to Oreo or Pie version of /e/OS we do an upgrade testing. We will announce which devices are going to be upgraded in the posts in [this category on our forum](https://community.e.foundation/c/e-devices/testing/128).For the duration of the upgrade testing we would create a testing team comprising of volunteer users. This team gets disbanded once the builds go live.

**Porting or Applications Testing** Occasionally we port /e/OS on devices which are not supported by other custom ROM's. This can be smartphones, laptops or tablets. For e.g. we recently ported [/e/OS to the FP3](https://doc.e.foundation/devices/FP3/) or we came out with [Olimex](https://doc.e.foundation/devices/olimex/) and [Pinebook](https://doc.e.foundation/devices/pinebook/) builds. The [easy-installer](https://community.e.foundation/c/community/easy-installer/136) was an application we created which required detailed testing during the development phase.

For the duration of the testing we would create a testing team comprising of volunteer users. This team gets disbanded once the builds go live.


### What is expected from a Tester

As a tester you would be required to

- Flash the test build on your device
- This will be a manual flash the first time . Subsequent builds will or should be available OTA
- Flashing your device will wipe out existing data on your phone. So please take a backup before proceeding
- Only those who have experience in flashing ROM’s need join
- Testing will only be done for devices in our supported list.

If your device is not in the supported list you can [request a device to be added](https://community.e.foundation/c/e-devices/request-a-device/76). This also means you should not join the testing team.

### Can I opt to leave the testing team?

Yes you can. Participation as a tester is a voluntary activity. You are free to leave the team any time you wish.
- Please note to return to the normal stable builds you will have to manually flash the stable builds on your device. This is because the test builds and development or stable builds are made with different keys.

### How do I apply?

Please send an email to <testing@e.email> with the below details


- [/e/ Gitlab username](https://gitlab.e.foundation/e) ( Required)
- Telegram username (Required)
- Device Name (Required)
- /e/ email ID (Optional)
- Tester Name (Optional)

### Why do we want these details?

  - /e/ Gitlab username: To add your name to the testing project. This will give you access to download the builds from our server
  - Telegram username  : To add your name to our telegram testing channel. This has to be in the format @name. Phone numbers do not work while adding to the channel!
  - Device name  : The name of the device you want to test for. It should be on our supported list. Unofficial builds will not come under the scope of our testing.
  - /e/ email ID : To add your name to an email distribution list for testers. If you do not have an /e/ email ID , send across an email ID which you access
  - Tester Name  : Optional. Your name if you want to share it.

### Testing process FAQ

- What happens if a build fails or bugs are detected in your testing?

If a build fails in the testing process we hold back the release of the build and it goes back to the developers. Similarly if a bug is reported in the testing we revert that fix to the previous working version and the bug goes back to the developer to be resolved.
- How do you handle such reverts in an agile development scenario?

As you may recall we have 2 week development cycles for a sprint. The bugs which are detected during the testing are moved to the next sprint cycle and only those fixes that are successful get into the build.
- Are only bugs fixed in these sprints what about enhancements?

That is a good question. We combine both enhancement as well as bug fixes in the sprints. If the enhancement related coding do not finish in the given timeline it is moved to the next sprint cycle.
- I do not have any experience of flashing ROM's neither is my device in the supported list can I join the testing team?

We appreciate your enthusiasm but Please do not join the testing team if this is the case . As we mentioned above the testing team is working against a strict timeline. It will not be possible to handhold a user to understand how to flash the ROM. For that level of assistance you can  [go to our forum.](https://community.e.foundation/)
