---
sidebar: home_sidebar
folder: upgrade
layout: page
title: /e/ OS Version Upgrade
permalink: /devices/enchilada/upgrade
device: enchilada
---
{% include templates/device_upgrade.md %}