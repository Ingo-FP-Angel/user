---
sidebar: home_sidebar
folder: upgrade
layout: page
title: /e/ OS Version Upgrade
permalink: /devices/cheeseburger/upgrade
device: cheeseburger
---
{% include templates/device_upgrade.md %}