---
sidebar: home_sidebar
folder: upgrade
layout: page
title: /e/ OS Version Upgrade
permalink: /devices/gemini/upgrade
device: gemini
---
{% include templates/device_upgrade.md %}