- Current app version: <version number we are based onto>
- Target app version: <the upstream release to use>
- Release note: <url to the release note of the upstream release>

## What's new?

<List what is new in the upstream update>

## Validation

<List test case to validate the the update is working as expected>

/label ~Task
