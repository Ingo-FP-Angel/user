---
layout: page
title: Validation
permalink: internal/development-process/dev/validation
search: exclude
---

## What ?

Validate the dev done for an issue

## Why ?

Detect as soon as possible errors and give feedbacks to dev

## Who ?

| People | Role |
| ------ | ---- |
|        |      |

## How ?

1. Read the test case describe in the MR
2. Download and install the APK build on GitLab, and validate that it's working as expected
3. Have a look into the code and maybe give feedbacks if needed
4. Once everything is ok, you can approve the MR
5. Once approve, assign again the MR to its author
6. The author is now allowed to merge
7. The issue should be automatically closed

## Where ?

MR

## When ?

When a developer has an MR assign to him which he did not develop

---

[Next (Run the test build) >](../test/run-test-build)
