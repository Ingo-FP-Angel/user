---
layout: page
title: Sprint
permalink: internal/development-process/prepare/sprint
search: exclude
---

## What ?

Prepare the sprints

## Why ?

The aim is to be able to set a rythm in development process, and facilitate the definition of a roadmap.

## Who ?

| People | Role |
| ------ | ---- |
|        |      |

## How ?

1. Choose the sprint name
    - Find a city starting
    - choose a picture representative of this city (ideally, a monument)
1. Create a milestone
    - give it a name
    - set the image as description
    - set a start and end date
1. Create a Telegram channel
    - Set the title as Sprint <city>
    - Add on it the sprint team
    - allow history for new members
    - set 2 admins
2. Assign issue to the sprints
    - pay attention to the weight of each issue
    - pay attention to the speciality of each developer working on the spring

## Where ?

GitLab

- the issue list
- milestone page https://gitlab.e.foundation/groups/e/-/milestones

## When ?

Before each sprint


créer la milestone
 - titre
 - photo en description
créer une conversation télégram
 - titre
 - image
 - activer l'historique pour les nouveaux membres
 - administrateur (RH, GD au minimum)

---

[Next (Issue assignment) >](../dev/issue-assignment)
