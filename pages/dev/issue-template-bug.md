- /e/ version:
- Device model:
- When it started to occur:
- Reproducible with the last /e/ version:
- Reproducible with LineageOS:

## Summary

<Summarize the bug encountered briefly and precisely>

<Please tick one of the following sentences if relevent>

- [ ] The device is unusable
- [ ] The bug is the source of a data loss or a big waste of time
- [ ] The bug concerns a tierce app
- [ ] The bug concerns security
- [ ] The bug concerns privacy

## The problem

**Steps to reproduce**

<How one can reproduce the issue>

**What is the current behavior?**

<What actually happens>

**What is the expected correct behavior?**

<What you should see instead>

## Technical informations

**Relevant logs (`adb logcat`)**

<Paste any relevant logs in the codeblock bellow>

```

```

**Relevant screenshots**

<Screenshots of the problem>

## Solutions

**Workaround**

<To get the feature working or at least to make the device usable>

**Possible fixes**

<Any idea to fix the issue or a link to the line of code that might be the cause for this problem>

/label ~Bug
