---
layout: page
title: Announcement
permalink: internal/development-process/release/announcement
search: exclude
---

## What ?

## Why ?

## Who ?

| People | Role |
| ------ | ---- |
|        |      |

## How ?

1. Share the resolution list on /e/ Developer channel
2. Create a community thread

## Where ?

## When ?

---

[Next (Closing the sprint) >](closing-the-sprint)
