---
layout: page
title: Release note
permalink: internal/development-process/release/release-note
search: exclude
---

## What ?

Write a release note

## Why ?

Share the next updates and the progress of the project

## Who ?

| People | Role |
| ------ | ---- |
|        |      |

## How ?

1. Take the list of sprint issues
   - ✅ fixed
   - 🆗 non reproducible
   - ⚠️ blocked
   - ❌ won't be done
2.

## Where ?

## When ?

---

[Next (Announcement) >](announcement)
