---
layout: page
title: How to factory reset my device ?
permalink: how-tos/factory-reset
---

## When my screen is not available

1. Install `adb` (doc [here](https://community.e.foundation/t/howto-installing-adb-and-fastboot-on-your-computer/3051))
1. Reboot the device into recovery
1. `adb shell twrp wipe data` (doc [here](https://twrp.me/faq/openrecoveryscript.html))

    > /!\ This command format only app data. Files are still present on internal storage.
    
1. `adb shell rm -rf /sdcard/*` remove files from internal storage
