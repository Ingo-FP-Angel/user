---
sidebar: home_sidebar
title: Build /e/ for Samsung Galaxy S7 - herolte
folder: build
layout: page
permalink: /devices/herolte/build
device: herolte
---
{% include templates/device_build.md %}
