---
sidebar: home_sidebar
title: Build /e/ for Motorola Moto Z - griffin
folder: build
layout: page
permalink: /devices/griffin/build
device: griffin
---
{% include templates/device_build.md %}