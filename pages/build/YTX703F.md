---
sidebar: home_sidebar
title: Build /e/ for Lenovo Yoga Tab 3 Plus - YTX703F
folder: build
layout: page
permalink: /devices/YTX703F/build
device: YTX703F
---
{% include templates/device_build.md %}