---
sidebar: home_sidebar
title: Build /e/ for BQ Aquaris X2 - zangya
folder: build
layout: page
permalink: /devices/zangya/build
device: zangya
---
{% include templates/device_build.md %}