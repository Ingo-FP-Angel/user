---
sidebar: home_sidebar
title: Build /e/ for Motorola Moto G 2015 - osprey
folder: build
layout: page
permalink: /devices/osprey/build
device: osprey
---
{% include templates/device_build.md %}
