---
sidebar: home_sidebar
title: Build /e/ for Asus Zenfone 3 (zenfone3)
folder: build
layout: page
permalink: /devices/zenfone3/build
device: zenfone3
---
{% include templates/device_build.md %}
