---
sidebar: home_sidebar
title: Build /e/ for Sony Xperia Z3 Compact - z3c
folder: build
layout: page
permalink: /devices/z3c/build
device: z3c
---
{% include templates/device_build.md %}