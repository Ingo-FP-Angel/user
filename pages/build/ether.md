---
sidebar: home_sidebar
title: Build /e/ for Nextbit Robin - ether
folder: build
layout: page
permalink: /devices/ether/build
device: ether
---
{% include templates/device_build.md %}