---
sidebar: home_sidebar
title: Build /e/ for Samsung Galaxy Note 9 - crownlte
folder: build
layout: page
permalink: /devices/crownlte/build
device: crownlte
---
{% include templates/device_build.md %}