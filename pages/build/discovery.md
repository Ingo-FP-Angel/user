---
sidebar: home_sidebar
title: Build /e/ for Sony Xperia XA2 Ultra - discovery
folder: build
layout: page
permalink: /devices/discovery/build
device: discovery
---
{% include templates/device_build.md %}