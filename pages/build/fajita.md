---
sidebar: home_sidebar
title: Build /e/ for OnePlus 6T - fajita
folder: build
layout: page
permalink: /devices/fajita/build
device: fajita
---
{% include templates/device_build.md %}
