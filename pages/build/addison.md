---
sidebar: home_sidebar
title: Build /e/ for Motorola Moto Z Play - addison
folder: build
layout: page
permalink: /devices/addison/build
device: addison
---
{% include templates/device_build.md %}
