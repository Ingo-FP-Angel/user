---
sidebar: home_sidebar
title: Build /e/ for Samsung Galaxy Tab S5e (Wi-Fi) - gts4lvwifi
folder: build
layout: page
permalink: /devices/gts4lvwifi/build
device: gts4lvwifi
---
{% include templates/device_build.md %}
