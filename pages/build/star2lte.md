---
sidebar: home_sidebar
title: Build /e/ for Samsung Galaxy S9+ - star2lte
folder: build
layout: page
permalink: /devices/star2lte/build
device: star2lte
---
{% include templates/device_build.md %}
