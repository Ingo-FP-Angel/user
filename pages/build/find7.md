---
sidebar: home_sidebar
title: Build /e/ for OPPO Find 7a/s - find7
folder: build
layout: page
permalink: /devices/find7/build
device: find7
---
{% include templates/device_build.md %}