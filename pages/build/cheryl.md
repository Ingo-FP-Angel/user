---
sidebar: home_sidebar
title: Build /e/ for Razer Phone cheryl
folder: build
layout: page
permalink: /devices/cheryl/build
device: cheryl
---
{% include templates/device_build.md %}