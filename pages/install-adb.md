---
layout: page
permalink: /pages/install-adb
search: exclude
toc: true
---
## Steps to Install adb on your PC

{% include alerts/danger.html content="The default adb installation on OS like Ubuntu can be outdated and can cause errors. We would recommend that linux distribution users ensure they use the installation process given here" %}

1.&nbsp;Download latest version of the **Platform Tools** specific to your Operating system from the [android site](https://developer.android.com/studio/releases/platform-tools) 

{% include alerts/tip.html content=" Windows users will need the proper drivers installed on their computer" %}

2.&nbsp;Extract the archive somewhere you can remember

3.&nbsp;Run the below command in a console
  
**For adb** 

```
path-to-the-extracted-archive/platform-tools/adb
```
...to the `adb` in the above path add the commands you want to execute for e.g. `adb devices`

**For fastboot** 
```
path-to-the-extracted-archive/platform-tools/fastboot
```
...to the `fastboot` in the above path add commands you want to execute for e.g `fastboot devices`

### Add the path to you PC configuration
To avoid having to type the above commands every time you can add the location of the folder where you save the extracted platform tool file to you system configuration
> On Ubuntu systems

Type the below command to you .bashrc file
>You will be able to see the .bashrc file which are hidden files by pressing on your keyboard <kbd>ctrl</kbd>and <kbd>h</kbd> keys

```
export PATH=$PATH:$HOME"/platform-tools"
export ANDROID_HOME=/platform-tools
export PATH=$PATH:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools
```
>here we have assumed you have saved the downloaded files to a folder named `platform-tools` and this is in your home directory

After this you should be able to access `adb` and `fastboot` by typing only the below commands in a console

`adb devices`

or 

`fastboot devices`