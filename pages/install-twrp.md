---
layout: page
title: Install TWRP
permalink: /pages/install-twrp
search: exclude
toc: true
---

## Pre requisites
- An `adb` enabled PC
- `USB debugging` enabled on your phone 
  
- To set up adb read [this document](install-adb)
- To enable USB debugging read [this document](enable-usb-debugging)

## Installation 

- Open a console screen in the PC and type the below command
```
adb devices
```
- This should display you device as detected on the screen. It will show up as an alphanumeric number followed by 'device' on your console. Check screenshot below

![](/images/adb_devices.png)

- If requested grant permission to “Allow USB debugging”, tap OK.
- Next type the below command in the console 
```
    adb reboot bootloader
```

- Once your device boots into bootloader mode, type this into the command line.
```
    fastboot flash recovery twrp-2.8.x.x-xxx.img
```
- In this command modify twrp.img with the name of your TWRP recovery .img file.

- Once TWRP is successfully flashed on your device, type this final command to reboot your device.
```
    fastboot reboot
```
- This should reboot your device back into the OS 