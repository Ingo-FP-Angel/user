---
sidebar: home_sidebar
title: Info about OnePlus X - onyx
folder: info
layout: default
permalink: /devices/onyx
device: onyx
---
{% include templates/device_info.md %}
