---
sidebar: home_sidebar
title: Info about Samsung Galaxy Tab S5e (Wi-Fi) - gts4lvwifi
folder: info
layout: default
permalink: /devices/gts4lvwifi
device: gts4lvwifi
---
{% include templates/device_info.md %}