---
sidebar: home_sidebar
title: Info about OnePlus 2 - oneplus2
folder: info
layout: default
permalink: /devices/oneplus2
device: oneplus2
---
{% include templates/device_info.md %}
