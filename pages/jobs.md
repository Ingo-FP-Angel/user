---
layout: page
title: Jobs @/e/
permalink: jobs
search: exclude
---

## Currently opened positions (remote):

Those are full time positions, remote working, part of a great team of multicultural and international people:
 - [ANDROID DEVELOPER] we are looking for great Android developers to work on various projects. Contact us (techjobs@e.email).
 - [ANDROID DEVELOPER] we are looking for high skilled developers who like to reverse-engineer complicated APIs like the Google Services. Hacker in mind? Contact us (techjobs@e.email).
 - [QA MANAGER] we are looking for an experienced QA manager for all our products. Can't sleep with bugs? Contact us (techjobs@e.email). 
 - [DESIGNER] we are looking for our awesome lead designer, who will help us to design the best mobile experience. Feeling like the new Jony Ive? Contact us (jobs@e.email).
 - [ECOMMERCE SALES MANAGER] we are looking for our eCommerce sales manager. In love with international online sales? Contact us (jobs@e.email).
 - [CUSTOMER SUPPORT AND AFTER SALES] we are looking for new customer support and after sales people. Love to help users with small or huge problem, always with a smile? :) Contact us (jobs@e.email).
 - [GRAPHIST] we are looking for experimented and talented graphists. Like to design awesome user interfaces and icons? Contact us (jobs@e.email).
 - [WEB DEVELOPER] HTML, CSS, JS, database, etc. etc. you know the story. Working fast and bug free? Contact us (techjobs@e.email).
 - [MARKETING PEOPLE] we are looking for marketing assistants & execs. Creative in mind? Contact us (jobs@e.email).
 - [PRIVACY SPECIALIST] we are looking for a specialist of privacy questions, both technically and legally speaking. Private by design? Contact us (jobs@e.email).
 - [ACCOUNTING ASSISTANT] we are looking for our accounting assistant who will take care of billing and payroll. Super picky and rigorous? Contact us (jobs@e.email).

In 2021 we are also looking for our CTO, CMO. Though those positions are not yet officially opened, if you have a good track record in your field, please feel free to contact us (jobs@e.email).

Good level of English reading/writing/speaking is mandatory for all positions.

## How to apply?

Please send an email to <techjobs@e.email> or <jobs@e.email> depending on the position, **along with a resume**.
