---
sidebar: home_sidebar
title: Install /e/ on PineBook Laptops
folder: install
layout: default
permalink: /devices/pinebook/install
device: pinebook
---
{% include templates/laptop_install.md %}
