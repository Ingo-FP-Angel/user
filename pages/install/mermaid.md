---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/mermaid/install
device: mermaid
---
{% include templates/device_install.md %}