---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/beryllium/install
device: beryllium
---
{% include templates/device_install.md %}
