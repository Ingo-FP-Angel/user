---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/bullhead/install
device: bullhead
---
{% include templates/device_install.md %}
