---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/lavender/install
device: lavender
---
{% include templates/device_install.md %}