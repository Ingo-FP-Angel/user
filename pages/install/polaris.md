---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/polaris/install
device: polaris
---
{% include templates/device_install.md %}
