---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/mako/install
device: mako
---
{% include templates/device_install.md %}
