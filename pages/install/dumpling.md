---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/dumpling/install
device: dumpling
---
{% include templates/device_install.md %}
