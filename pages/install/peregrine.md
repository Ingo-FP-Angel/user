---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/peregrine/install
device: peregrine
---
{% include templates/device_install.md %}
