---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/onyx/install
device: onyx
---
{% include templates/device_install.md %}
