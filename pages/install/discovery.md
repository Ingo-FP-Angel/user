---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/discovery/install
device: discovery
---
{% include templates/device_install.md %}